These are scripts written for the Investigative Marine Biology Laboratory course at Shoals Marine Lab.

Faculty:
Doug Fudge - University of Guelph
Denny Taylor - Hiram College

Teaching Assistants:
Andrew Swafford - University of California, Santa Barbara
Sarah Boggett - University of Guelph
Sarah Schorno - University of Guelph

Scripts will be listed as:

[Class Year]-[Title]-[Author]
eg.
2016-R_Introduction-ASwafford

Titles sould be descriptive.